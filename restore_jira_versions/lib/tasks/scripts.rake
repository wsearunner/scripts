require 'csv'

namespace :scripts do
  desc "TODO"
  task inserts: :environment do
    puts "generating INSERT statements for JIRA versions"

    base_insert_sql = <<-SQL
INSERT INTO nodeassociation (SOURCE_NODE_ID, SOURCE_NODE_ENTITY, SINK_NODE_ID, SINK_NODE_ENTITY, ASSOCIATION_TYPE)
  SELECT ISSUE_ID, 'Issue', VERSION_ID, 'Version', 'ASSOC_TYPE'
    FROM DUAL
    WHERE NOT EXISTS (SELECT *
                        FROM nodeassociation
                        WHERE SOURCE_NODE_ID     = ISSUE_ID
                          AND SOURCE_NODE_ENTITY = 'Issue'
                          AND SINK_NODE_ID       = VERSION_ID
                          AND SINK_NODE_ENTITY   = 'Version'
                          AND ASSOCIATION_TYPE   = 'ASSOC_TYPE');
SQL

    mapping_hash = { }
    sql_dir      = "#{ Rails.root }/tmp/sql"

    # hash the version mapping
    CSV.foreach("#{ Rails.root }/doc/csv/version_mapping.csv", :headers => true) do |row|
      mapping_hash[row['BACKUP_ID']] = row['LIVE_ID']
    end

    # create / clear SQL scripts dir
    FileUtils.mkdir_p sql_dir
    FileUtils.rm_rf "#{ sql_dir }/*"

    last_version_id = nil # we're going to use this to track when we need to create a new SQL file
    sql_file        = nil

    # iterate the issue / version associatoins and build individual INSERTs
    CSV.foreach("#{ Rails.root }/doc/csv/issue_version_relation_BACKUP.csv", :headers => true) do |row|
      issue_id   = row['SOURCE_NODE_ID']
      version_id = row['SINK_NODE_ID']
      assoc_type = row['ASSOCIATION_TYPE']

      mapped_version_id = mapping_hash[version_id]

      # sub in issue_id, new version_id, association type
      local_insert_sql = base_insert_sql.
        gsub(/ISSUE_ID/, issue_id).
        gsub(/VERSION_ID/, mapped_version_id).
        gsub(/ASSOC_TYPE/, assoc_type)

      if last_version_id != version_id
        # create new SQL file
        sql_file = File.new("#{ sql_dir }/#{ version_id }.sql", "w")
        last_version_id = version_id

        puts ".. creating #{ version_id }.sql"
      end

      sql_file.write(local_insert_sql)
    end

    puts "finished with INSERT statements"
  end

end
